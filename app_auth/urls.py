
from django.conf.urls import url,include
from app_auth import views


urlpatterns = [
    url('^login/', views.Login),
    url('^logout/', views.Logout),
]

