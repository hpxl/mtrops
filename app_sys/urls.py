
from django.conf.urls import url,include
from app_sys import views


urlpatterns = [
    url('^nessus/$', views.Nessus),
    url('^webssh/$', views.Webssh),
    url('^upfile/$', views.Upfile),
    url('^downfile/$', views.Downfile),
]

