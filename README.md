# mtrops
本屌丝也是个小运维，开发运维平台目的是为了提高工作效率（其实是偷懒），此项目在持续开发中，如果你有好的建议请在下方留言。


#### 平台架构 ####
![输入图片说明](https://gitee.com/uploads/images/2018/0621/103400_15f96904_578265.png "屏幕截图.png")

#### 安装依赖 ####

1. django==1.11.9
2. ELK
3. webssh
4. openpyxl
5. saltstack,salt.client
6. Mysql-python

#### 使用说明 ####

1. 平台新建需通过django manage 创建管理用户进行登录验证

2. 权限控制方面未完善，首次创建需要插入一些数据
   A，权限管理初始数据
   B，角色管理数据
   C，管理用户初始化权限数据





#### 参与贡献 ####

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 项目介绍 ####
![登录界面](https://gitee.com/uploads/images/2018/0620/150427_c7b3a516_578265.png "屏幕截图.png")
![首页](https://gitee.com/uploads/images/2018/0620/150506_d455c153_578265.png "屏幕截图.png")
![资产管理](https://gitee.com/uploads/images/2018/0620/150531_dd01efa8_578265.png "屏幕截图.png")
![站点管理](https://gitee.com/uploads/images/2018/0620/150653_fcaac1cb_578265.png "屏幕截图.png")
![权限管理](https://gitee.com/uploads/images/2018/0620/150728_4abde699_578265.png "屏幕截图.png")
![webshell](https://gitee.com/uploads/images/2018/0620/150808_f676accc_578265.png "屏幕截图.png")
