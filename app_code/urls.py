
from django.conf.urls import url,include
from app_code import views


urlpatterns = [
    url('^project/$', views.Project),
    url('^editproject/$', views.EditProject),
    url('^delproject/$', views.DelProject),
    url('^addproject/$', views.AddProject),


    url('^site/$', views.Site),
    url('^editsite/$', views.EditSite),
    url('^addsite/$', views.AddSite),
    url('^delsite/$', views.DelSite),

    url('^postcode/page_id=(\d+)/$', views.PostCode),
    url('^postcode/$', views.PostCode),
    url('^filtersite/$', views.FilterSite),
    url('^addpost/$', views.AddPost),
    url('^upcode/$', views.UpCode),
    url('^rollback/$', views.RollBack),
    url('^delpost/$', views.DelPost),
    url('^recordlog/post_id=(\d+)/$', views.RecordLog),


    url('^depend/$', views.Depend),
    url('^editdepend/$', views.EditDepend),
    url('^deldepend/$', views.DelDepend),
    url('^addepend/$', views.AddDepend),
    url('^install/$', views.Install),
]

