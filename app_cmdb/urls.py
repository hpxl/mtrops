
from django.conf.urls import url,include
from app_cmdb import views


urlpatterns = [

    url('^hostinfo/?page_id=(\d+)/$', views.HostInfo),
    url('^hostinfo/$', views.HostInfo),
    url('^addhost/$', views.AddHost),
    url('^delhost/$', views.DelHost),
    url('^synchost/$', views.SyncHost),
    url('^detailinfo/ip=(\d+.\d+.\d+.\d+)/$', views.Detailinfo),
    url('^filterhost/$', views.FilterHost),
    url('^expcmdb/$', views.ExpCmdb),
    url('^edithost/$',views.EditHost),


    url('^loginuser/$',views.LoginUser),
    url('^addlguser/$',views.AddLoginUser),
    url('^dellguser/$',views.DelLoginUser),
    url('^editlguser/$',views.EditLoginUser),





    url('^idc/$', views.Idc),
    url('^editidc/$', views.EditIdc),
    url('^delidc/$', views.DelIdc),
    url('^addidc/$', views.AddIdc),

    url('^hostgroup/$', views.HostGroup),
    url('^addgroup/$', views.AddGroup),
    url('^editgroup/$',views.EditGroup),
    url('^delgroup/$',views.DelGroup),

]

