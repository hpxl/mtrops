# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-04-08 01:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HostGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('group_name', models.CharField(max_length=64, unique=True)),
                ('msg', models.CharField(max_length=128, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='HostInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('IP', models.CharField(max_length=64, unique=True)),
                ('username', models.CharField(max_length=64)),
                ('passwd', models.CharField(max_length=256)),
                ('device_type', models.CharField(max_length=64)),
                ('port', models.CharField(max_length=64)),
                ('msg', models.CharField(max_length=64, null=True)),
                ('hostname', models.CharField(max_length=64, null=True)),
                ('in_ip', models.CharField(max_length=64, null=True)),
                ('os_type', models.CharField(max_length=64, null=True)),
                ('service_type', models.CharField(max_length=64, null=True)),
                ('os_version', models.CharField(max_length=64, null=True)),
                ('mem_total', models.CharField(max_length=64, null=True)),
                ('mem_swap', models.CharField(default=b'0', max_length=32)),
                ('cpu_type', models.CharField(max_length=64, null=True)),
                ('cpu_num', models.CharField(max_length=16, null=True)),
                ('mount_hone', models.CharField(max_length=128, null=True)),
                ('mount_root', models.CharField(max_length=128, null=True)),
                ('mount_alidata', models.CharField(max_length=128, null=True)),
                ('kernel', models.CharField(max_length=64, null=True)),
                ('status', models.CharField(max_length=16, null=True)),
                ('addTime', models.DateTimeField(auto_now_add=True)),
                ('host_group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_cmdb.HostGroup')),
            ],
        ),
        migrations.CreateModel(
            name='IDC',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('idc_name', models.CharField(max_length=64, unique=True)),
                ('msg', models.CharField(max_length=128, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='hostinfo',
            name='idc',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_cmdb.IDC'),
        ),
    ]
