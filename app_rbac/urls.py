
from django.conf.urls import url,include
from app_rbac import views


urlpatterns = [
    url('^usermg/$', views.UserMg),
    url('^adduser/$', views.AddUser),
    url('^edituser/$', views.EditUser),
    url('^deluser/$', views.DelUser),

    url('^perms/page_id=(\d+)/$', views.Permission),
    url('^perms/$', views.Permission),

    url('^addperms/$', views.AddPermission),
    url('^editperms/$', views.EditPerms),
    url('^delperms/$', views.DelPerms),

    url('^role/$', views.Role),
    url('^addrole/$', views.AddRole),
    url('^editrole/$', views.EditRole),
    url('^delrole/$', views.DelRole),
    url('^roleperms/$', views.RolePerms),
    url('^addroleperms/$', views.AddRolePerms),

    url('^menu/page_id=(\d+)/$', views.Menu),
    url('^menu/$', views.Menu),

]

