input { 
	#系统
	file {
		path => "/var/log/messages"
		type => "syslog_messages"
		}

	file {
		path => "/var/log/usermonitor/usermonitor.log"
		type => "syslog_usermonitor"
		}
	
	file {
		path => "/var/log/secure"
		type => "syslog_secure"
		}
			
	#木头人	
	file {
		path => "/alidata/manage.mtrp2p.com/log/*/*.log"
		type => "manage.mtrp2p.com"
		}
		
	file {
		path => "/alidata/price.mtrp2p.com/log/*/*.log"
		type => "price.mtrp2p.com"
		}

		
	file {
		path => "/alidata/services/message/logs/*/*.log"
		type => "rpc_message"
		}
		
	file {
		path => "/alidata/admin.mtrp2p.com/log/*/*.log"
		type => "admin.mtrp2p.com"
		}
	
	file {
		path => "/alidata/m.mtrp2p.com/log/*/*.log"
		type => "m.mtrp2p.com"
		}
		
	file {
		path => "/alidata/rpc.mtrp2p.com/log/*/*.log"
		type => "rpc.mtrp2p.com"
		}
	
	file {
		path => "/alidata/www.mtrp2p.com/log/*/*.log"
		type => "www.mtrp2p.com"
		}	
	
	#木头云交易
	file {
		path => "/alidata/www.mutouyun.com/log/*/*.log"
		type => "www.mutouyun.com"
		}
	file {
		path => "/alidata/wap.mutouyun.com/log/*/*.log"
		type => "wap.mutouyun.com"
		}
	#木头云
	file {
		path => "/alidata/admin.mutouyun.com/log/*/*.log"
		type => "admin.mutouyun.com"
		}
	file {
		path => "/alidata/m.mutouyun.com/log/*/*.log"
		type => "m.mutouyun.com"
		}
	file {
		path => "/alidata/ww.mutouyun.com/log/*/*.log"
		type => "ww.mutouyun.com"
		}
	}


filter {

	mutate {
		# 替换元数据host的值
		replace => ["host", "192.168.1.192"]
	}
}


output {
	elasticsearch { 
		hosts => ["192.168.1.88:9200"] 
		index => "%{type}"
		}
		
	#终端标准输出
	#stdout { codec => rubydebug }
}
